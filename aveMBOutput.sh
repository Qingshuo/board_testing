#!/bin/bash
#aveMBOutput.sh
name=`head -1 gainLog0`
awk 'BEGIN{for(i=0;i<8;i++){a1[i]=0;a2[i]=0;a3[i]=0;a4[i]=0;a5[i]=0;a6[i]=0;a7[i]=0;cnt=0;}}//{
if(cnt<1){
}
else if(cnt<11){
for(i=0;i<8;i++){
a1[i]+=$(2+2*i);
}}
else if(cnt<21){
for(i=0;i<8;i++){
a2[i]+=$(2+2*i);
}}
else if(cnt<31){
for(i=0;i<8;i++){
a3[i]+=$(2+2*i);
}}
else if(cnt<41){
for(i=0;i<8;i++){
a4[i]+=$(2+2*i);
}}
else if(cnt<51){
for(i=0;i<8;i++){
a5[i]+=$(2+2*i);
}}
else if(cnt<61){
for(i=0;i<8;i++){
a6[i]+=$(2+2*i);
}}
else if(cnt<71){
for(i=0;i<8;i++){
a7[i]+=$(2+2*i);
}}
cnt++;}
END{for(i=0;i<8;i++){printf "%.3f ", a1[i]/10}
printf "\n";
for(i=0;i<8;i++){printf "%.3f ", a2[i]/10}
printf "\n";
for(i=0;i<8;i++){printf "%.3f ", a3[i]/10}
printf "\n";
for(i=0;i<8;i++){printf "%.3f ", a4[i]/10}
printf "\n";
for(i=0;i<8;i++){printf "%.3f ", a5[i]/10}
printf "\n";
for(i=0;i<8;i++){printf "%.3f ", a6[i]/10}
printf "\n";
for(i=0;i<8;i++){printf "%.3f ", a7[i]/10}
printf "\n";}' ~/exp_trace/gainLog0 > ~/exp_trace/MB${name}
echo "MB:${name}"
cat ~/exp_trace/MB${name}
echo ""

