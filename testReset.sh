#!/bin/bash
#testReset.sh


now=$(date +"%Y_%m_%d_%H_%M")
#sudo mv /tmp/sensingLog "/home/pi/schedule/sensingLog$now"
#cd /home/pi/schedule/
#tar -jcvf "/home/pi/schedule/sensing/sensingLog$now.tar.bz2" "sensingLog$now"
#sudo rm "/home/pi/schedule/sensingLog$now"
sudo rm /tmp/sensingLog
sudo rm ~/exp_trace/resetTmp
sleep 2
lines=0
sleep 50
lines=`grep "DB1 RMS" /tmp/sensingLog | wc -l`
echo $lines
grep "DB1 Gain:" /tmp/sensingLog | tail -1
#if [ $lines -gt 50 ]
#then
grep "DB1 RMS" /tmp/sensingLog  >> ~/exp_trace/resetTmp
awk 'BEGIN{for(i=0;i<12;i++){a[i]=0;}}//{
for(i=0;i<12;i++){
a[i]+=$(5+2*i);
}
}
END{
for(i=0;i<12;i++){printf ("[%d]%.3f ",i+1, a[i]/NR)}
printf "\n";
}' ~/exp_trace/resetTmp
#fi
