#!/bin/bash
#testMBNoise.sh


now=$(date +"%Y_%m_%d_%H_%M")
sudo rm /tmp/sensingLog
sudo rm ~/exp_trace/noiseTmp
sleep 2
lines=0
sleep 100
lines=`grep "DB0 RMS" /tmp/sensingLog | wc -l`
echo $lines
echo "MB noise:"
grep "DB0 RMS" /tmp/sensingLog  >> ~/exp_trace/noiseTmp
cat noiseTmp
awk 'BEGIN{for(i=0;i<8;i++){a[i]=0;}}//{
for(i=0;i<8;i++){
a[i]+=$(5+2*i);
}
}
END{
for(i=0;i<8;i++){printf ("[%d]%.3f ",i+1, a[i]/NR)}
printf "\n";
}' ~/exp_trace/noiseTmp


