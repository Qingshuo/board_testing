#!/bin/bash
#test6Reset.sh
sudo rm /tmp/sensingLog
sleep 2
lines=0
sleep 10
lines=`grep "DB1 RMS" /tmp/sensingLog | wc -l`
echo $lines
i=1
while [ $i -le $1 ]
do
echo "DB: $i"
grep "DB$i Gain:" /tmp/sensingLog | tail -1
sudo rm ~/exp_trace/resetTmp
grep "DB$i RMS" /tmp/sensingLog  >> ~/exp_trace/resetTmp
awk 'BEGIN{for(i=0;i<15;i++){a[i]=0;}}//{
for(i=0;i<15;i++){
a[i]+=$(5+2*i);
}
}
END{
for(i=0;i<15;i++){printf ("[%d]%.3f ",i+1, a[i]/NR)}
printf "\n";
}' ~/exp_trace/resetTmp
echo ""
let i=$i+1
done
