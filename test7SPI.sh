#!/bin/bash
#test7SPI.sh

sudo rm /mnt/rd/sensingLogProc
i=0
while [ $i -le $1 ]
do
	sudo rm /home/pi/exp_trace/SPITmp$i
	let i=$i+1
done
sleep 2
lines=0
sleep 540
let i=0
lines=`grep "DB0 ByteError" /tmp/sensingLogProc | wc -l`
echo "total samples: $lines"	
while [ $i -le $1 ]
do
echo "DB$i :"
grep "DB$i ByteError" /mnt/rd/sensingLogProc  >> /home/pi/exp_trace/SPITmp$i
awk 'BEGIN{cnt=0;}//{
if($9!=0)
{
cnt++;
print $0;
}
}
END{
if(cnt==0){
print "no byte error"}
else{
print cnt/2, "samples have byte error";}
}' /home/pi/exp_trace/SPITmp$i
let i=$i+1
done

