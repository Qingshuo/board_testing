#!/bin/bash
#finalTestReset.sh

lines=0
lines2=0
count=0
sudo rm "/tmp/sensingLog"
sudo rm ~/exp_trace/DBResetResult
sudo rm ~/exp_trace/DBResetAve
sleep 2

lines=`grep "reset: 1 " /tmp/sensingLog | wc -l`
while [ $lines -lt 1 ]
do
#	sleep 0.01
    lines=`grep "reset: 1 " /tmp/sensingLog | wc -l`
done
lines2=`grep "DB1 RMS" /tmp/sensingLog | wc -l`
echo "found reset 1: line$lines2"

sleep 50
echo "20%"
sleep 50
echo "40%"
sleep 50
echo "60%"
sleep 50
echo "80%"
sleep 50

lines=`grep "DB1 RMS" /tmp/sensingLog | wc -l`
let lines3=$lines2+249
echo "expected no. of samples:$lines3"
echo "received lines now: $lines"
while [ $lines -lt $lines3 ]
do
    sleep 1
    lines=`grep "DB1 RMS" /tmp/sensingLog | wc -l`
done

grep "DB1 RMS"  /tmp/sensingLog >> ~/exp_trace/DBResetResult

awk -v row="$lines2" 'BEGIN{for(i=0;i<12;i++){a1[i]=0;a5[i]=0;a10[i]=0;a50[i]=0;a100[i]=0;cnt=1;}}//{
if(cnt<row){}
else if(cnt<row+50){
for(i=0;i<12;i++){
a1[i]+=$(5+2*i);
}}
else if(cnt<row+100){
for(i=0;i<12;i++){
a5[i]+=$(5+2*i);
}}
else if(cnt<row+150){
for(i=0;i<12;i++){
a10[i]+=$(5+2*i);
}}
else if(cnt<row+200){
for(i=0;i<12;i++){
a50[i]+=$(5+2*i);
}}
else if(cnt<row+250){
for(i=0;i<12;i++){
a100[i]+=$(5+2*i);
}}
cnt++;}
END{printf "No. of lines obtained: %d\n", NR;
for(i=0;i<12;i++){printf "%.3f ", a1[i]/50}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a5[i]/50}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a10[i]/50}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a50[i]/50}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a100[i]/50}
printf "\n";}' ~/exp_trace/DBResetResult > ~/exp_trace/DBResetAve

cat ~/exp_trace/DBResetAve

#echo " "

#now="$year-$month-$day-$hour-$minute"
#sudo mv /tmp/sensingLog /home/pi/exp_trace/sensingLog
#sudo mv /tmp/dbgLog /home/pi/exp_trace/dbgLog
#cd /home/pi/exp_trace/
#tar -jcvf "testDBGain.tar.bz2" "sensingLog" "dbgLog" "DBResetResult" "DBResetAve"
#ssh amposerver@192.168.0.2 "mkdir ~/test_reset/$now"
#scp "testDBGain.tar.bz2" "amposerver@192.168.0.2:~/test_reset/$now"
#rm  "testDBGain.tar.bz2"
#sleep 5
#ssh amposerver@192.168.0.2 "~/test_gain/./saveFigure.sh $hour $minute $second 2"




#let count1=1
#while [ $count1 -lt 41 ]
#do

#	ssh amposerver@192.168.0.2 "cp /var/www/html/ampo/pi54/16/$month/$day/$hour/figures/${time[$count1]}*  ~/test_gain/$now"
#	let count1=$count1+1
#done


