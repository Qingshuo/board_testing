#!/bin/bash
#testDBNoise.sh


now=$(date +"%Y_%m_%d_%H_%M")
sudo rm /tmp/sensingLog
sudo rm ~/exp_trace/noiseTmp
sleep 2
lines=0
sleep 100
lines=`grep "DB1 RMS" /tmp/sensingLog | wc -l`
echo $lines
echo "DB noise:"
grep "DB1 RMS" /tmp/sensingLog  >> ~/exp_trace/noiseTmp
awk 'BEGIN{for(i=0;i<12;i++){a[i]=0;}}//{
for(i=0;i<12;i++){
a[i]+=$(5+2*i);
}
}
END{
for(i=0;i<12;i++){printf ("[%d]%.3f ",i+1, a[i]/NR)}
printf "\n";
}' ~/exp_trace/noiseTmp


