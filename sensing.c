//master version after starmb
#include <pthread.h> 
#include <wiringPiSPI.h>  
#include <wiringPi.h>  
#include <stdint.h>  
#include <unistd.h>  
#include <stdio.h>  
#include <stdlib.h>  
#include <getopt.h>  
#include <stdint.h>  
#include <sys/time.h>  
#include <string.h>  
#include <errno.h>
#include <math.h>
#include "sensing.h"

Control_Buff *System_State_Buff;
Control_Buff_MB *System_State_Buff_MB;


uint8_t Active_DB=0; //Total daughter boards connected  
//I put buff_len+5 here to ease debugging. Sometimes I intentially read more from SPI to make sure DMA SPI NDTR is correctly set. So I can see some repetition at the end.
//uint8_t buff[Max_DB][buff_len+5] = {0xff};//The local buffer that collect data from the spi lines of the daughter baord.  
uint8_t buff[Max_DB+1][buff_len] = {{0xff}};//The local buffer that collect data from the spi lines of the daughter baord.
  
uint16_t buffRaw[SAMPLES_DB/2][ CHANNELS_TOTAL ]={{0}};
double RMS[ CHANNELS_TOTAL ]={0};

uint16_t gainTable[8]={0,1,2,5,10,20,50,100};//according to the datasheet of LTC 6910

uint16_t temp1[CHANNELS_TOTAL]={0};  
uint16_t temp3[CHANNELS_TOTAL]={0};  // To check new AMR-assignment

//const uint8_t AMRnoAtChannel[CHANNELS_TOTAL] = {
//const uint8_t AMRnoAtChannel[15] = {//this arrary is defined by DMA fetching sequence. See AMR_Adc.c in firmware, DMA fetch ADC1, ADC2, ADC3 in sequence, however at each ADC conversion, they are not in order of AMR1, AMR2, AMR3. 
	//this is because not all channel have access to all ADC, so only some channels access ADC3 will be place at wrong position( with respect to AMR order ), read RM0090 manual ADC chapter
	//TripleADC mode, DMA sequence, and AMR_Adc.c channel definition
//	0, 1, 2, 3, 4 , 9, 5, 6, 10, 7, 8, 11, 12, 13, 14
//};
const uint8_t AMRnoAtChannel[9] = {//this arrary is defined by DMA fetching sequence. See AMR_Adc.c in firmware, DMA fetch ADC1, ADC2, ADC3 in sequence, however at each ADC conversion, they are not in order of AMR1, AMR2, AMR3. 
	2, 3, 0, 4, 5, 1, 6, 7, 8
};
uint32_t Global_increment=0;  
uint8_t end =0;  
uint8_t No_Sensor_DB[Max_DB]={0};    
uint16_t dataStart=0;  

const char* DB_Port_Names[] = { " j6\r\n", " j7\r\n"," j8\r\n", " j9\r\n"," j10\r\n"," j12\r\n" };  
const char* DB_Port_Names_L[] = { " j6\n", " j7\n"," j8\n", " j9\n"," j10\n"," j12\n" };

const char* DB_CT_Port[] = { "isens1", "isens2","isens3" };  
const uint8_t DB_Port_pins[] = {NSS_J6,NSS_J7,NSS_J8,NSS_J9,NSS_J10,NSS_J12};
const uint8_t DB_Port_NRSTs[ ]={NRST_J6, NRST_J7,NRST_J8,NRST_J9,NRST_J10,NRST_J12};
uint8_t DB_Port_In_Use[Max_DB] = {0};  
FILE  *file_d[Max_DB], *file_dbg, *file_Volt, *file_Curr, *gofile, *file_tmp[Max_DB], *file_Log, *file_Ct_dbg, *Test_Gain_Log, *File_Name_Log;
FILE *file_filtered_d[ Max_DB ], *file_filtered_Curr;

char globalLog[10000],globalDbg[ 10000];

uint8_t logDbgFile = 1;
uint8_t sensingLog = 1;

uint8_t gainChanged=0;
uint8_t wrongGain=0;
int posi[Max_DB+1]={0};
uint8_t sampleInterval=5;
uint8_t master=0;
time_t pretime=0;
struct timeval tv;
time_t curtime;
suseconds_t curtimeu;
uint8_t testReset=0;
uint8_t testGain[Max_DB+1]={0};
uint8_t gainCnt=0;
uint8_t gain2=0;
uint8_t gain3=0;
uint8_t gain4=0;
uint8_t gain5=0;
uint8_t testGainStart=0;
char fileName[500];
uint8_t disableADC[Max_DB+1]={0};
double filteredData[SAMPLES_DB/2][CHANNELS_TOTAL]={{0.0}};
char* DB_Port_Names_R[Max_DB] = { " j6", " j7"," j8", " j9"," j10"," j12" };  
char* DB_Port_Name[Max_DB];
//the GPIO pins for SPI_NSS for all Daughter boards have to be initialized
void Init_GPIO()
{
	int i=0;
	
		/*Set the respective GPIOs as output and initialize them
	  All NSS pins are set as output and ports are held high
	  the Synch_Intr pin is held low */
	  
	pinMode (Synch_Interrupt, OUTPUT); //synch intr
	pinMode (DATA_READY, INPUT); //signal from MB to let rpi start spi
	pullUpDnControl(DATA_READY,PUD_DOWN);//data_ready pin is pulled down
	//pinMode (DATA_READY, OUTPUT); //signal from MB to let rpi start spi
	//digitalWrite(DATA_READY,0);
	pinMode (NSS_MB,OUTPUT); // NSS MB
	pinMode (NRST_MB,OUTPUT); // NSS MB
	
	digitalWrite(NSS_MB,1);
	digitalWrite(NRST_MB,1);
	
	digitalWrite(Synch_Interrupt,0);

	//ensure all SPI line are high
	for(i=0; i<6; i++)   //qs i<Max_DB
	{
	  pinMode (DB_Port_pins[i],OUTPUT); // NSS pins of active DB
	  pinMode (DB_Port_NRSTs[i],OUTPUT); // NSS pins of active DB
	  digitalWrite(DB_Port_pins[i],1);
	  digitalWrite(DB_Port_NRSTs[i],1);
	}
		  pinMode (USB_DEMUX1,OUTPUT); // USB pin
		  pinMode (USB_DEMUX2,OUTPUT); // USB pin
		  pinMode (USB_DEMUX3,OUTPUT); // USB pin
}

//This function sets all the amplifiers in a given board to the value in 'gain'
void Set_gain_all(Control_Buff *Temp_buff,int gain)
{
	int i;
	for( i=0;i<CHANNELS_TOTAL;i++)
		Temp_buff->Channel_Gain[i]=gain;
}

//Sets the gain of a specific channel
void Set_gain(Control_Buff *Temp_buff,int gain, int channel)
{
	Temp_buff->Channel_Gain[channel]=gain;
}
/*
//the reset function imrpoves sensitivity of the AMR sensors
void Reset_all(Control_Buff *Temp_buff)
{
  Temp_buff->Reset=1;
}

//Once the Reset_all function is called and the data passed over spi line this function should be called
void Clear_Reset_all(Control_Buff *Temp_buff)
{
	Temp_buff->Reset=0;
}
*/
//Read the system parameters from the conf file
void get_system_Param()
{
    FILE  *Sys_param;
	char buf[1000];
	int line_no =0, k=0;
	char *ptr;

	file_Log=fopen("/tmp/sensingLog", "a");
	fprintf(file_Log,"amr.conf:\n");		
	
	Sys_param=fopen("/home/pi/ampo/conf/amr.conf","r");
    
	while (fgets(buf,1000, Sys_param)!=NULL)
	{
			
		fprintf(file_Log,"%s",buf);		

		line_no++;
		if(line_no==3)
		{
			Active_DB = strtol(buf, &ptr, 10);			
		}
		
		if(line_no >3 && line_no < 4 + Active_DB)
		{ 
		   char * temp;
		   //		   int test=0;
		   int l=0;
		   if(strlen(buf) < 3)
		   continue;
		   
		    No_Sensor_DB[line_no-4]=strtol(buf, &temp , 10);		   
	
			for(l=0;l<6;l++)//qs l<Max_DB
			{
				
				if(strcmp(temp,DB_Port_Names[l]) == 0 || strcmp(temp,DB_Port_Names_L[l]) == 0)
				{
					DB_Port_In_Use[line_no-4]= DB_Port_pins[l];
					DB_Port_Name[line_no-4]=DB_Port_Names_R[l];
					break;
				}
			}	
		}
		//log_dbg_file
		if(line_no==(4+Active_DB*2) ){
			logDbgFile = strtol(buf, &ptr, 10 );
			
		}
		if(line_no==(5+Active_DB*2) ){
		    sensingLog = strtol(buf, &ptr, 10 );
			
		}
		if(line_no==(6+Active_DB*2) ){
			sampleInterval = strtol( buf, &ptr, 10);
		}
		if(line_no==(7+Active_DB*2) ){
			master = strtol( buf, &ptr, 10);
		}
		if(line_no==(8+Active_DB*2) ){
			testReset = strtol( buf, &ptr, 10);
		}

    }
	
	printf("Reading conf/amr.conf. \n  Number of daughter boards: %d\n",Active_DB);
	
	
	for(k=0;k<Active_DB;k++)
		printf("  Daughter board %d at port %d, with %d sensors\n", k+1, DB_Port_In_Use[k],No_Sensor_DB[k]);

	
	if( logDbgFile==1 ){
		printf("LOG DBG FILE is defined\n");
		fprintf(file_Log,"LOG DBG FILE is defined\n");		

	}
	else{
		printf("LOG DBG FILE is not defined\n");
		fprintf(file_Log,"LOG DBG FILE is not defined\n");		
	}
		
	if( sensingLog==1 ){
		printf("sensingLog is defined\n");		
	}
	else{
		printf("sensingLog is not defined\n");		
	}
	if( master==1 ){
		printf("MASTER MODE\n");
		fprintf(file_Log,"MASTER MODE\n");
		
		if( sampleInterval>=1){
		     printf( "sample interval is %ds\n",sampleInterval);
	    }
		else{
		     printf( "wrong sample interval: %ds\n",sampleInterval);
		     sampleInterval=5;
	    }
	}
	else{
		printf("SLAVE MODE\n");
		fprintf(file_Log,"SLAVE MODE\n");		

	}
	if( testReset==1 ){
		printf("RMS divided by gain\n");		
	}

		
	fclose(Sys_param);
	printf("Processing sensor readings.\n");
	fclose(file_Log );
	
}
void Create_Log_Files()
{
	struct tm *tm_struct ;
	char buffer[40], buffer1[40], logPath[49];// dbgFileName[40];

	gettimeofday(&tv,NULL);
	curtime=tv.tv_sec;

	tm_struct = localtime(&curtime);
	strftime(buffer,30,"%Y-%m-%d",tm_struct);

	sprintf(buffer1, "%s-%02d-%02d-%02d-00", buffer,tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec);
	
	if( sensingLog==1){
		sprintf(logPath, "/tmp/sensingLog");
		file_Log=fopen(logPath, "a");
		fprintf(file_Log,"\n********************* log for %s \n", buffer1);		
	}		

		
}
void Create_Files()
{
	struct tm *tm_struct ;
	char Path[40],buffer[40],buffer1[40], tmpPath[40],  dbgPath[40];// dbgFileName[40];
	int i;

	gettimeofday(&tv,NULL);
	curtime=tv.tv_sec;

	tm_struct = localtime(&curtime);
	strftime(buffer,30,"%Y-%m-%d",tm_struct);

	sprintf(buffer1, "/mnt/rd/ampo/%s-%02d-%02d-%02d-00", buffer,tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec);
	sprintf(dbgPath, "/tmp/%s-%02d-%02d-%02d-00", buffer,tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec);
	sprintf(fileName,"%s-%02d-%02d-%02d-00", buffer,tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec);

//sprintf(buffer1, "../data/%s-%02d-%02d-%02d", buffer,tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec);
		 
//	#ifdef LOG
	//if( sensingLog==1){
		

	//	#endif
	//}

	sprintf(dbgPath, "/tmp/dbgLog");
	file_dbg=fopen(dbgPath, "a");
	fprintf(file_dbg,"\n*********** dbg log for %s \n", buffer1);		
	fprintf(file_Log,"%02d-%02d-%02d-%02d  data file created: %s \n", tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec,(int)(tv.tv_usec/10000),buffer1);		


	
    //Open new Curr data file    	
	sprintf(Path, "%s-CTs", buffer1);
	//qs	sprintf(tmpPath, "%s-fCTs", buffer1);

	// printf("%s\n", Path);
	file_Curr=fopen(Path,"w");
	//qs	file_filtered_Curr=fopen(tmpPath,"w"); 	 
	//	sprintf(Path, "%s-CTs.dbg", dbgPath);  	  

	 


	 //Open new DB data file
	for ( i = 0; i < Active_DB; i++ ) 
    {    	
      sprintf(Path, "%s-AMR-%d",buffer1,i+1);  	  
	  //      sprintf(dbgFileName, "%s-AMR-%d.dbg",dbgPath,i+1);  	  
	  //      sprintf(tmpPath, "%s-tmp-%d", buffer1, i+1);
	  //qs	  sprintf(tmpPath, "%s-fAMR-%d", buffer1, i+1);

	  file_d[i]=fopen(Path,"w");
	  //qs	  file_filtered_d[i]=fopen(tmpPath,"w");  
	}

	
	
	//Open new Volt data file    	
     sprintf(Path, "%s-Volt", buffer1); 
     // printf("%s\n", Path);	 
	 file_Volt=fopen(Path,"w"); 

    
}

void writeLog(char * msg){
	if( sensingLog==1){
		struct timeval tv;
		time_t curtime;
		struct tm *tm_struct ;
		char bufferLog[5000];

		gettimeofday(&tv,NULL);
		curtime=tv.tv_sec;
		tm_struct = localtime(&curtime);
		sprintf(bufferLog, "%02d-%02d-%02d-%02d  %s", tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec,(int)(tv.tv_usec/10000), msg);
  
		fprintf(file_Log,"%s \n", bufferLog);		
		fflush(file_Log);
	}
}

void writeDbgLog(char * msg){
  struct timeval tv;
  time_t curtime;
  struct tm *tm_struct ;
  char bufferLog[5000];

  gettimeofday(&tv,NULL);
  curtime=tv.tv_sec;
  tm_struct = localtime(&curtime);
  sprintf(bufferLog, "%02d-%02d-%02d-%02d  %s", tm_struct->tm_hour,tm_struct->tm_min,tm_struct->tm_sec,(int)(tv.tv_usec/10000), msg);
  
  fprintf(file_dbg,"%s \n", bufferLog);		
  fflush(file_dbg);

}


void Close_files()
{
	 int i;
	
  for ( i = 0; i < Active_DB; i++ ) 
    {           	  
      fclose(file_d[i]); 	  
    }
  fclose(file_Volt);
  fclose(file_Curr);

  //#ifdef LOG
  if( sensingLog==1){
		fclose(file_Log);
	}
  //#endif

  fclose( file_dbg);

}

void FIRFilter( int AMRno){
	int No_Sensor;
	int i=0, j=0,k=0;
	//	double coeff[ 21]={0.011,0.012,0.013,0.014,0.015,0.016,0.017,0.018,0.019,0.02,0.021,0.022,0.023,0.024,0.025,0.026,0.027,0.028,0.029,0.030,0.031};
double coeff[ 301 ]={0.00049, 0.00049, 0.00050, 0.00050, 0.00050, 0.00051, 0.00052, 0.00052, 0.00053, 0.00054, 0.00056, 0.00057, 0.00058, 0.00060, 0.00061, 0.00063, 0.00065, 0.00067, 0.00069, 0.00072, 0.00074, 0.00076, 0.00079, 0.00082, 0.00084, 0.00087, 0.00090, 0.00094, 0.00097, 0.00100, 0.00104, 0.00107, 0.00111, 0.00114, 0.00118, 0.00122, 0.00126, 0.00130, 0.00135, 0.00139, 0.00143, 0.00148, 0.00152, 0.00157, 0.00162, 0.00166, 0.00171, 0.00176, 0.00181, 0.00186, 0.00191, 0.00196, 0.00202, 0.00207, 0.00212, 0.00218, 0.00223, 0.00229, 0.00234, 0.00240, 0.00245, 0.00251, 0.00257, 0.00263, 0.00268, 0.00274, 0.00280, 0.00286, 0.00292, 0.00298, 0.00304, 0.00309, 0.00315, 0.00321, 0.00327, 0.00333, 0.00339, 0.00345, 0.00351, 0.00357, 0.00363, 0.00369, 0.00375, 0.00380, 0.00386, 0.00392, 0.00398, 0.00404, 0.00409, 0.00415, 0.00421, 0.00427, 0.00432, 0.00438, 0.00443, 0.00449, 0.00454, 0.00459, 0.00465, 0.00470, 0.00475, 0.00480, 0.00485, 0.00490, 0.00495, 0.00500, 0.00505, 0.00509, 0.00514, 0.00519, 0.00523, 0.00527, 0.00532, 0.00536, 0.00540, 0.00544, 0.00548, 0.00552, 0.00556, 0.00559, 0.00563, 0.00566, 0.00570, 0.00573, 0.00576, 0.00579, 0.00582, 0.00585, 0.00587, 0.00590, 0.00592, 0.00595, 0.00597, 0.00599, 0.00601, 0.00603, 0.00605, 0.00607, 0.00608, 0.00609, 0.00611, 0.00612, 0.00613, 0.00614, 0.00615, 0.00615, 0.00616, 0.00616, 0.00617, 0.00617, 0.00617, 0.00617, 0.00617, 0.00616, 0.00616, 0.00615, 0.00615, 0.00614, 0.00613, 0.00612, 0.00611, 0.00609, 0.00608, 0.00607, 0.00605, 0.00603, 0.00601, 0.00599, 0.00597, 0.00595, 0.00592, 0.00590, 0.00587, 0.00585, 0.00582, 0.00579, 0.00576, 0.00573, 0.00570, 0.00566, 0.00563, 0.00559, 0.00556, 0.00552, 0.00548, 0.00544, 0.00540, 0.00536, 0.00532, 0.00527, 0.00523, 0.00519, 0.00514, 0.00509, 0.00505, 0.00500, 0.00495, 0.00490, 0.00485, 0.00480, 0.00475, 0.00470, 0.00465, 0.00459, 0.00454, 0.00449, 0.00443, 0.00438, 0.00432, 0.00427, 0.00421, 0.00415, 0.00409, 0.00404, 0.00398, 0.00392, 0.00386, 0.00380, 0.00375, 0.00369, 0.00363, 0.00357, 0.00351, 0.00345, 0.00339, 0.00333, 0.00327, 0.00321, 0.00315, 0.00309, 0.00304, 0.00298, 0.00292, 0.00286, 0.00280, 0.00274, 0.00268, 0.00263, 0.00257, 0.00251, 0.00245, 0.00240, 0.00234, 0.00229, 0.00223, 0.00218, 0.00212, 0.00207, 0.00202, 0.00196, 0.00191, 0.00186, 0.00181, 0.00176, 0.00171, 0.00166, 0.00162, 0.00157, 0.00152, 0.00148, 0.00143, 0.00139, 0.00135, 0.00130, 0.00126, 0.00122, 0.00118, 0.00114, 0.00111, 0.00107, 0.00104, 0.00100, 0.00097, 0.00094, 0.00090, 0.00087, 0.00084, 0.00082, 0.00079, 0.00076, 0.00074, 0.00072, 0.00069, 0.00067, 0.00065, 0.00063, 0.00061, 0.00060, 0.00058, 0.00057, 0.00056, 0.00054, 0.00053, 0.00052, 0.00052, 0.00051, 0.00050, 0.00050, 0.00050, 0.00049, 0.00049 };
	if( AMRno!=0){//write AMR files
		No_Sensor=CHANNELS_TOTAL;
	}
	else{
		No_Sensor=Total_CT;
	}
	
	for ( i=0;i<No_Sensor;i++){
		for (j=301;j<(SAMPLES_DB/2);j++){
			//		for (j=10;j<4790;j++){
			for( k=0;k<301;k++){
				filteredData[j][i] = filteredData[j][i] + coeff[k]*(double)System_State_Buff->Sampled_data[j-k][i];  
			}
		}
	}
}

int calBer(int AMRno, int dataStartIdx ){

	int i=0,j=0;
	uint32_t byteError=0;
	uint16_t *data;
	uint16_t pre=13;
	uint16_t cur=14;
	//	int m=AMRno-1;// this is the index of AMRno in buffer
	data=&System_State_Buff->Channel_Gain[CHANNELS_TOTAL];  
		
	for (i=0;i<(DATA_LEN_DB/2);i++)
		{
			cur=data[i];
			if( (cur-pre)!=1){
				byteError++;
				if( j==0){
					sprintf( globalLog, "DB%d ByteError Found at [%d]%d",AMRno, i , data[i]);					
				}
				else if( j<10){
					sprintf( globalLog, "%s  [%d]%d",globalLog, i, data[i] );					
				}
				j++;
			}
			pre=cur;
		}
	if( j!=0){
		logIt;
	}
	/*	for (i=dataStartIdx+GAIN_LEN;i<dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB;i++){//for every byte in buffer
		if( buff[ AMRno ][ i ]!=0xaa ){
		byteError++;
		}
		}*/

	sprintf( globalLog, "DB%d ByteError %d\n", AMRno, byteError );
	logIt;

	return byteError;
}

void calRMS(int channel ){  //input: No_Sensor_DB
	int i=0,j=0,SAMPLE_SIZE=SAMPLES_DB/2;
	double mean=0,rms=0;

	for( i=0;i<channel;i++){
		mean=0;
		for(j=0;j<SAMPLE_SIZE;j++ ){//each data is divided by sample size and then added to mean. This can avoid large summation
			mean +=(double)System_State_Buff->Sampled_data[j][i]/(double)SAMPLE_SIZE;
		}
		rms=0;
		for(j=0;j<SAMPLE_SIZE;j++ ){//calculate rms from mean
			rms+=( (double)System_State_Buff->Sampled_data[j][i]-mean )*( (double)System_State_Buff->Sampled_data[j][i]-mean );
		}
		rms=rms/SAMPLE_SIZE;
		rms=sqrt( rms );
		RMS[i]=rms;
	}

	/*	for( i=0;i<channel;i++){
		sprintf(globalLog, "%s [%d] %.2f ", globalLog, i,RMS[i]);
	}
	//    sprintf(globalLog, "%sresetValue\n", globalLog);   
    logIt;
	*/

}

void writeInDBGFiles(int AMRno, int dataStartIdx){//the input AMRno is buffer index, thus starts from 1.
	int n=0, No_Sensor=0, counterValue, verValue, resetValue,gain[ CHANNELS_TOTAL]={0};

	if( dataStartIdx==-1 ){
		sprintf(globalLog, "AMRno %d cannot Find start\n", AMRno);
		logIt;
		return;
	}

	if( AMRno==0){
		System_State_Buff_MB=( Control_Buff_MB *) &buff[ AMRno][ dataStartIdx];
		verValue=System_State_Buff_MB -> version;
	}
	else{
		System_State_Buff=( Control_Buff *) &buff[ AMRno][ dataStartIdx];
		verValue=System_State_Buff -> version;
	}

	if((verValue>>15)%2==1)
	{
		sprintf(globalLog, "Board Type: Mother Board");
		logIt;
	}
	else if((verValue>>12)%2==1){
		sprintf(globalLog, "Board Type: Star Danghter Board");
		logIt;
	}
	else{
		sprintf(globalLog, "Board Type: Danghter Board");
		logIt;
	}
	sprintf(globalLog, "Board ID: %04d", verValue%256);
	logIt;
	
	if( AMRno!=0){
	sprintf(globalLog, "Port: %s", DB_Port_Name[AMRno-1]);
	logIt;
	}
	
	if((verValue>>14)%2==1)
	{
		sprintf(globalLog, "CAL_BER");
		logIt;
	}
	if((verValue>>13)%2==1)
	{
		disableADC[AMRno]=1;
		sprintf(globalLog, "ADC Disabled");
		logIt;
	}
	else{
		disableADC[AMRno]=0;
	}
	if((verValue>>11)%2==1)
	{
		testGain[AMRno]=1;
		sprintf(globalLog, "Test Gain Mode");
		logIt;
	}
	else{
		testGain[AMRno]=0;
	}

	if((verValue>>10)%2==1)
	{
		sprintf(globalLog, "Test Reset Mode");
		logIt;
	}


	if((verValue>>9)%2==1)
	{
		sprintf(globalLog, "reset 10 times at each interrupt");
		logIt;
	}
	

  
  

  
	if( AMRno!=0){//write AMR files
		No_Sensor=CHANNELS_TOTAL;

		logIt;
	  	sprintf(globalLog, "verValue %04x buff[%d][+3] %02x buff[%d][+2] %02x", verValue,AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB+1], AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB]);
		logIt;
		counterValue=System_State_Buff -> counter;
		resetValue=System_State_Buff -> numReset;
		sprintf(globalLog, "reset: %d  ", resetValue);		
		sprintf(globalLog, "counterValue %d, buff[%d][+3] %02x buff[%d][+2] %02x", counterValue,AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB+3], AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB+2]);
		logIt;
		
	    for (n=0;n<No_Sensor;n++){
			if(System_State_Buff -> Channel_Gain[n]>0 && System_State_Buff -> Channel_Gain[n]<8){
				gain[n]=gainTable[System_State_Buff -> Channel_Gain[n]];
			}
			else{
				sprintf(globalLog, "%dth gain incorrect, with value %d \n", n,System_State_Buff -> Channel_Gain[n]);
				wrongGain=1;
				gain[n]=1;//gain inital value is 0, divided by 0 gain will result in problem
			}
		}
		sprintf(globalLog, "DB%d Gain:",AMRno);
		sprintf(globalDbg, "DB%d Gain:",AMRno);
		for (n=0;n<(No_Sensor);n++){
			sprintf(globalLog, "%s[%d]%d \t\t",globalLog, n+1,System_State_Buff -> Channel_Gain[n]);
			sprintf(globalDbg, "%s[%d]%d \t\t",globalDbg, n+1,System_State_Buff -> Channel_Gain[n]);	  
			if( System_State_Buff -> reasonOne[ n]!=0 || System_State_Buff -> reasonTwo[ n]!=0 || System_State_Buff -> reasonThree[ n]!=0 ){
				gainChanged=1;
			}
		}
		logIt;
		dbgIt;
		if( logDbgFile==1 ){
			sprintf( globalDbg, "file_dbg[%d]:\n",AMRno);
			for (n=0;n<6*(CHANNELS_TOTAL);n++){
				if( (n%(CHANNELS_TOTAL))<No_Sensor){
					sprintf( globalDbg, "%s%d \t",globalDbg, System_State_Buff -> channelMax[n]);
				}
				if( (n+1)%(CHANNELS_TOTAL)==0)
					sprintf( globalDbg, "%s\n",globalDbg);
			}
			dbgIt;
		}
		
	}
	else{
		No_Sensor=Total_CT;
	  	sprintf(globalLog, "verValue %04x buff[%d][+3] %02x buff[%d][+2] %02x", verValue,AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_MB+1], AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_MB]);
		logIt;
		counterValue=System_State_Buff_MB -> counter;
		
		sprintf(globalLog, "counterValue %d, buff[%d][+3] %02x buff[%d][+2] %02x", counterValue,AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB+3], AMRno, buff[AMRno][dataStartIdx+GAIN_DATA_LEN_WITH_DBG_DB+2]);
		logIt;
		
	    for (n=0;n<No_Sensor;n++){
			if(System_State_Buff_MB -> Channel_Gain[n]>0 && System_State_Buff_MB -> Channel_Gain[n]<8){
				gain[n]=gainTable[System_State_Buff_MB -> Channel_Gain[n]];
			}
			else{
				sprintf(globalLog, "%dth gain incorrect, with value %d \n", n,System_State_Buff_MB -> Channel_Gain[n]);
				wrongGain=1;
				gain[n]=1;//gain inital value is 0, divided by 0 gain will result in problem
			}
		}
		sprintf(globalLog, "DB%d Gain:",AMRno);
		sprintf(globalDbg, "DB%d Gain:",AMRno);
		for (n=0;n<(No_Sensor);n++){
			sprintf(globalLog, "%s[%d]%d \t\t",globalLog, n+1,System_State_Buff_MB -> Channel_Gain[n]);
			sprintf(globalDbg, "%s[%d]%d \t\t",globalDbg, n+1,System_State_Buff_MB -> Channel_Gain[n]);	  
			if( System_State_Buff_MB -> reasonOne[ n]!=0 || System_State_Buff_MB -> reasonTwo[ n]!=0 || System_State_Buff_MB -> reasonThree[ n]!=0 ){
				gainChanged=1;
			}
		}
		logIt;
		dbgIt;
		if( logDbgFile==1 ){
			sprintf( globalDbg, "file_dbg[%d]:\n",AMRno);
			for (n=0;n<6*(Total_CT);n++){
				if( (n%(Total_CT))<No_Sensor){
					sprintf( globalDbg, "%s%d \t",globalDbg, System_State_Buff_MB -> channelMax[n]);
				}
				if( (n+1)%(Total_CT)==0){
					sprintf( globalDbg, "%s\n",globalDbg);
				}
			}
			dbgIt;
		}
		
	}


  //sprintf(globalLog, "resetValue %d", resetValue);
  /*
  calRMS(No_Sensor );//calculate RMS of collected data
  
  sprintf( globalDbg, "DB%d RMS: ",AMRno);
  sprintf( globalLog, "DB%d RMS: ",AMRno);
  
  
  for (n=0;n<(No_Sensor);n++){
	  if( testReset==1){
	  //	  if( 0){
	  sprintf(globalDbg, "%s[%d] %.2lf \t", globalDbg, n+1,RMS[n]/(double)gain[n]);	
	  sprintf(globalLog, "%s[%d] %.2lf \t", globalLog, n+1,RMS[n]/(double)gain[n]);
	  }
	  else{
	  sprintf(globalDbg, "%s[%d] %.2lf \t", globalDbg, n+1,RMS[n]);///(double)gain[n]);	
	  sprintf(globalLog, "%s[%d] %.2lf \t", globalLog, n+1,RMS[n]);///(double)gain[n]);
	  }
  }
  
  sprintf(globalDbg, "%s\n", globalDbg);	
  logIt;
  dbgIt;

 */ 
    //test gain funciont
  //can only support one board in test Gain mode 
  /*if( testGain[AMRno]==1){
	  if(testGainStart==1){
		  if(System_State_Buff -> Channel_Gain[0]!=2){
			  testGainStart=0;
		  }

	  }
	  else if( System_State_Buff -> Channel_Gain[0]==2){
		  testGainStart=1;
		  gainCnt=0;
		  gain2=0;
		  gain3=0;
		  gain4=0;
		  gain5=0;
	  }

	  if( gainCnt==0){
		  Test_Gain_Log=fopen("/home/pi/exp_trace/gainLog", "w");
		  File_Name_Log=fopen("/home/pi/exp_trace/nameLog", "w");
	  }
	  if(gainCnt>0 && gainCnt<40){
		  Test_Gain_Log=fopen("/home/pi/exp_trace/gainLog", "a");
		  File_Name_Log=fopen("/home/pi/exp_trace/nameLog", "a");
		  
	  }
	  
	  if(gainCnt>=0 && gainCnt<40){
		  for (n=0;n<(No_Sensor);n++){
			  fprintf(Test_Gain_Log, "[%d] %.2lf \t",n+1,RMS[n]);
		  }

		  if(  System_State_Buff -> Channel_Gain[0]==2){
			  gain2++;
			  printf("gain2: %d\t",gain2);
		  }
		  if(  System_State_Buff -> Channel_Gain[0]==3){
			  gain3++;
			  printf("gain3: %d\t",gain3);
		  }
		  if(  System_State_Buff -> Channel_Gain[0]==4){
			  gain4++;
			  printf("gain4: %d\t",gain4);
		  }
		  if(  System_State_Buff -> Channel_Gain[0]==5){
			  gain5++;
			  printf("gain5: %d\t",gain5);
		  }
		  
		  fprintf(Test_Gain_Log,"\n");
		  fprintf(File_Name_Log,"%s\n",fileName);
		  fclose(Test_Gain_Log );
		  fclose(File_Name_Log);
		  printf("gain count: %d\n",gainCnt+1);
		  gainCnt++;
	  }


	  if( gainCnt==40){
		  printf( "testGain finish\n");
		  gainCnt++;
	  }
  }
*/
}


void writeInDataFiles(int AMRno, int dataStartIdx){//the input AMRno is buffer index, thus starts from 1.
	int n=0, m=0,i=0;
    m=AMRno-1;//this is the file hanlder index
	uint16_t *data;
  if( dataStartIdx==-1 ){
	  sprintf(globalLog, "AMRno %d cannot Find start\n", AMRno);
	  logIt;
	  return;
  }
  if(disableADC[AMRno]==1){
	  sprintf(globalLog, "DB%d ADC Disabled. No datafile written", AMRno);
	  logIt;
	  return;
  }
  //qs	System_State_Buff=( Control_Buff *) &buff[ AMRno][ dataStartIdx];
  //qs	data=&System_State_Buff->Channel_Gain[0];

  if( AMRno!=0){//write AMR files
	  System_State_Buff=( Control_Buff *) &buff[ AMRno][ dataStartIdx];
	  data=&System_State_Buff->Channel_Gain[0];
	  	  for (n=0;n<(DATA_LEN_DB/2+CHANNELS_TOTAL);n++)
		  {
			  /*	qs		  if( (n+1)<CHANNELS_TOTAL){
				  fprintf(file_filtered_d[m],"%d \t", data[n] );
			  }else{
				  fprintf(file_filtered_d[m],"%.5f \t", filteredData[n][AMRno] );				  
			  }*/
			  fprintf(file_d[m],"%d \t", data[n] );
			  if( n==8){
				  fprintf(file_d[m],"%d \t", 1 );
				  fprintf(file_d[m],"%d \t", 1 );
				  fprintf(file_d[m],"%d \t", 1 );
			  }
			  else if( n%9==8){
				  fprintf(file_d[m],"%d \t", 0 );
				  fprintf(file_d[m],"%d \t", 0 );
				  fprintf(file_d[m],"%d \t", 0 );
			  }

			  if( data[n]>4095 && i==0){
				  sprintf(globalLog, "wrong data found with index: %d", n);
				  logIt;
				  i=1;
			  }
			  if( (n+1)%(CHANNELS_TOTAL)==0){
				  fprintf(file_d[m],"\n");
				  //qs				  fprintf(file_filtered_d[m],"\n");				  
			  }
		  }

	  sprintf(globalLog, "Writing into DB%d file is completed", AMRno);
	  logIt;

  }else{//write CT files
	  System_State_Buff_MB=( Control_Buff_MB *) &buff[ AMRno][ dataStartIdx];
	  data=&System_State_Buff_MB->Channel_Gain[0];
	  for (n=0;n<(DATA_LEN_MB/2+Total_CT);n++)
		  {
				  
			  fprintf(file_Curr,"%d \t", data[n] );
			  if( n==2){
				  fprintf(file_Curr,"%d \t", data[n] );
				  fprintf(file_Curr,"%d \t", data[n] );
				  fprintf(file_Curr,"%d \t", data[n] );
				  fprintf(file_Curr,"%d \t", data[n] );
				  fprintf(file_Curr,"%d \t", data[n] );
			  }
			  else if( n%3==2){
				  fprintf(file_Curr,"%d \t", data[n] );
				  fprintf(file_Curr,"%d \t", 0 );
				  fprintf(file_Curr,"%d \t", 0 );
				  fprintf(file_Curr,"%d \t", 0 );
				  fprintf(file_Curr,"%d \t", 0 );				  
			  }
			  if( data[n]>4095 && i==0){
				  sprintf(globalLog, "wrong data found with index: %d", n);
				  logIt;
				  i=1;
			  }

			  if( (n+1)%(Total_CT)==0){
				fprintf(file_Curr,"\n");
				//				fprintf(file_filtered_Curr,"\n");				  
			  }
		  }
	  sprintf(globalLog, "Writing into MB file is completed");
	  logIt;

  }//end of MB file		
  
}

int findDataStart( uint8_t *dataBuff, int m ){//return the start of data, excluding the header
	int position=-1, i=0, dataBodyLen=0;//32 is 15*2byte gains +2byte reset

	if( m==0)
		dataBodyLen=GAIN_DATA_LEN_WITH_DBG_MB+4;
	else
		dataBodyLen=GAIN_DATA_LEN_WITH_DBG_DB+4;

	for( i=0;i<30;i++){//search the first 30 bytes, if cannot find the headerBytes, the data should be not complete, hence return -1.
			sprintf(globalLog, "\n " );
			sprintf(globalLog, "[%d]%02x [%d]%02x", i, dataBuff[ i ],i+1+dataBodyLen+1, dataBuff[ i+1+dataBodyLen+1]);		
			logIt;
			//if( dataBuff[ i ]==headerBYTE3&&dataBuff[ i+1 ]==headerBYTE4 &&dataBuff[ i+1+dataBodyLen+1]==footerBYTE1&&dataBuff[ i+1+dataBodyLen+2]==footerBYTE2/*&&dataBuff[ i+1+dataBodyLen+3]==footerBYTE3&&dataBuff[ i+1+dataBodyLen+4]==footerBYTE4*/){			
			if( dataBuff[ i ]==headerBYTE3&&dataBuff[ i+1 ]==headerBYTE4 ){
				position = i+2;
				if(dataBuff[ i+1+dataBodyLen+1]!=footerBYTE1||dataBuff[ i+1+dataBodyLen+2]!=footerBYTE2/*&&dataBuff[ i+1+dataBodyLen+3]==footerBYTE3&&dataBuff[ i+1+dataBodyLen+4]==footerBYTE4*/)
					{

						sprintf(globalLog, "DB%d cannot find footer", m);
						logIt;
					}
			break;
		}
	}
	sprintf(globalLog, "dataBodyLen %d, get position %d", dataBodyLen, position);		
	logIt;
	//	position=1;
	return position;
}

//demux function to choose the apropriate
void demux( int i ){


	if(  i==NSS_J6){

		digitalWrite( USB_DEMUX1,0);
		digitalWrite( USB_DEMUX2,0);
		digitalWrite( USB_DEMUX3,0);
		sprintf( globalLog, "demux 1A");
		logIt;
	}
	else if(  i==NSS_J7){

		digitalWrite( USB_DEMUX1,1);
		digitalWrite( USB_DEMUX2,0);
		digitalWrite( USB_DEMUX3,0);
		sprintf( globalLog, "demux 1B");
		logIt;
	}
	else if(  i==NSS_J8){

		digitalWrite( USB_DEMUX1,0);
		digitalWrite( USB_DEMUX2,1);
		digitalWrite( USB_DEMUX3,0);
		sprintf( globalLog, "demux 2A");
		logIt;
	}
	else if(  i==NSS_J9){

		digitalWrite( USB_DEMUX1,1);
		digitalWrite( USB_DEMUX2,1);
		digitalWrite( USB_DEMUX3,0);
		sprintf( globalLog, "demux 2B");
		logIt;
	}
	else if(  i==NSS_J10){

		digitalWrite( USB_DEMUX1,0);
		digitalWrite( USB_DEMUX2,0);
		digitalWrite( USB_DEMUX3,1);
		sprintf( globalLog, "demux 3A");
		logIt;
	}
	else if(  i==NSS_J12){

		digitalWrite( USB_DEMUX1,1);
		digitalWrite( USB_DEMUX2,0);
		digitalWrite( USB_DEMUX3,1);
		sprintf( globalLog, "demux 3B");
		logIt;
	}
	else if(  i==DEMUX_UNUSE){

		digitalWrite( USB_DEMUX1,0);
		digitalWrite( USB_DEMUX2,1);
		digitalWrite( USB_DEMUX3,1);
		sprintf( globalLog, "demux unused state");
		logIt;
	}
	else{

		digitalWrite( USB_DEMUX1,1);
		digitalWrite( USB_DEMUX2,1);
		digitalWrite( USB_DEMUX3,1);
		sprintf( globalLog, "wrong demux index\n");
		logIt;
	}
}



void test( ){
	printf( "start test\n");
	
	digitalWrite(NSS_J12,0);
	digitalWrite(NRST_J12,0);
	digitalWrite(NRST_J8,0);	
	digitalWrite(NRST_J9,0);

	digitalWrite(USB_DEMUX1,0);
	digitalWrite(USB_DEMUX2,0);
	digitalWrite(USB_DEMUX3,0);
}


void sendInterrupt( ){
		// send synchronization signals to the DB to start ADC sampling		
		sprintf(globalLog, "start to set 1 on INT pin");
		logIt;

		//		logIt;
		digitalWrite(Synch_Interrupt,1); 
		delay(10);	

		digitalWrite(Synch_Interrupt,0);
		//qs	delay(100);//wait for all ADC complete their work	
		sprintf(globalLog, "end of set 0 on INT pin, before SPIread, errno %d, %s", errno, strerror( errno));  
		logIt;
		//		logIt;
}

void receiveData( ){
	int i=0,k=0,p=0,ret,chan=0;
		for(i=0;i<Active_DB+1;i++)
		{
			if(i!=0)//it is daughterboard, not motherboard
			{
				sprintf(globalLog, "start of %dth activeDB", i);  //i changed to i-1
				logIt;

				demux(DB_Port_In_Use[i-1]);

				digitalWrite(DB_Port_In_Use[i-1],0);//i changed to i-1  //the NSS line should be held low before initiating a SPI transaction
			}
			else
			{
				sprintf(globalLog, "start of MB setting NSS_MB 0");  
				logIt;
				//				logIt;
				demux( DEMUX_UNUSE);
				digitalWrite(NSS_MB,0);
	
			}
			for( p=0;p<SPI_RX_TIMES;p++){//divide it to 4 times to reduce the return time, since read SPI has a timeout feature
				if( i!=0)
					ret=wiringPiSPIDataRW (chan,(unsigned char*)&buff[i][(buff_len*p/SPI_RX_TIMES) ],buff_len/SPI_RX_TIMES) ; //i+1 is changed to i //the data in buff_len is transfered to the DB. Once this call ends the buff[] contains data
				else
					ret=wiringPiSPIDataRW (chan,(unsigned char*)&buff[i][(buff_len_mb*p/SPI_RX_TIMES) ],buff_len_mb/SPI_RX_TIMES) ; //i+1 is changed to i //the data in buff_len is transfered to the DB. Once this call ends the buff[] contains data
				sprintf(globalLog, "buff_len/%d received %d bytes, errno=%d, %s", SPI_RX_TIMES, ret, errno ,strerror( errno));
				logIt;
				errno=0;
			}

			//			ret=wiringPiSPIDataRW (chan,(unsigned char*)&buff[i+1][(buff_len) ],6) ; //make sure SPI last byte is transferred xxx


			sprintf(globalLog, "DB%d header:\n",i);

			for(k=0;k<DEBUG_PRINT_BYTES;k++){
				sprintf(globalLog, "%s [%d]%02x", globalLog,k, buff[i][k]);//i+1 changed to i
			}

			sprintf(globalLog, "%s\nDB%d footer:", globalLog,i );

			for(k=0;k<DEBUG_PRINT_BYTES;k++){
				if( i!=0)
				sprintf(globalLog, "%s [%d]%02x", globalLog, buff_len-DEBUG_PRINT_BYTES+k, buff[i][buff_len-DEBUG_PRINT_BYTES+k]);//i+1 changed to i
				else
				sprintf(globalLog, "%s [%d]%02x", globalLog, buff_len_mb-DEBUG_PRINT_BYTES+k, buff[i][buff_len_mb-DEBUG_PRINT_BYTES+k]);//i+1 changed to i					
			}
			logIt;			

			sprintf(globalLog, "SPI of %dth DB complete, received %d bytes each, %d times\n", i, ret,SPI_RX_TIMES);// i changed to i-1  
			//			printf( "db end here2");

			if( i!=0)//it is daughterboard, not motherboard
			{

				digitalWrite(DB_Port_In_Use[i-1],1); //i changed to i-1 //once the SPI trasaction is over NSS line should be held high
				demux( DEMUX_UNUSE);
				sprintf(globalLog, "DB %dth set back NSS to 1\n", i);  //i changed to i-1

			}
			else
			{
				digitalWrite(NSS_MB,1);
				sprintf(globalLog, "MB set back NSS_MB 1\n");
			}
			logIt;
		//	printf("received data\n");
		}

}


void processData( ){
	int m=0;

		for(m=0;m<(Active_DB+1);m++)
		{
			sprintf(globalLog, "Processing DB%d of total%d", m,Active_DB);  
			logIt;
			
			posi[m]=findDataStart(buff[ m ],m );
		  writeInDBGFiles(m, posi[m]);
		  //qs 		  calBer( m, posi[m]);
		  // sprintf(globalLog, "time before filter");  
		  //logIt;
		  //	  FIRFilter( m);
		  //sprintf(globalLog, "time after filter");  
		  //logIt;
		}

		if( 1)//qs
					//if( gainChanged!=1 && wrongGain!=1)//no gains are changed, write files
		{
			for( m=0;m<(Active_DB+1);m++){
				writeInDataFiles(m,posi[m]);
			}
		}
		else{
			if( gainChanged==1)
				{
					sprintf(globalLog, "Gain is changed. Data abandoned \n");  
					logIt;
					gainChanged=0;
				}
			if( wrongGain==1)
				{
					sprintf(globalLog, "Wrong gain is detected. Data abandoned \n");  
					logIt;
					wrongGain=0;
				}
		}

}

void runMaster( ){

	while(  1 ){
		gettimeofday( &tv,NULL);
		curtime=tv.tv_sec;
		curtimeu=tv.tv_usec;
		if( sampleInterval == 1){
			//	if ( curtime>pretime)
			if ( (curtimeu%1000000)>=990000)
				{
					//pretime=curtime;
					delay ( 10);
					break;
				}
			
		}
		else if(  curtime%sampleInterval==0)//interrupt every 10 seconds
			break;
		
		usleep(  4000);
		
	}
	

	//	Create_Files();	
	sendInterrupt( );
	
	
	sprintf(globalLog, "Data_ready: %d",digitalRead(DATA_READY));
	logIt;
	//if the data ready wire is connected, we run the while loop and if loop
	
	while( 1){
		if(digitalRead(DATA_READY)==1){
			sprintf(globalLog, "Data ready detected");
			logIt;
			break;
		}
		
		gettimeofday( &tv,NULL);
		curtime=tv.tv_sec;
		curtimeu=tv.tv_usec;
		//sprintf(globalLog, "curtime: %d",curtime);
		//logIt;
		
		if( sampleInterval!=1){
			
			if(  curtime%sampleInterval==(sampleInterval-1)){
				sprintf(globalLog, "Data still not ready after %ds. Give up.",(sampleInterval-1));
				logIt;
				break;
			}
		}
		else{
		
		    if ( (curtimeu%1000000)>=990000){
		        sprintf(globalLog, "Data still not ready after 1s. Give up.");
		        logIt;
		        break;
	        }
		
	    }
		
	}
	
	if( digitalRead(DATA_READY)==1){
		Create_Files();
		receiveData( );
		processData( );
	}
	
	
	//if the data ready wire is not connected, we need to run this version:
	/*
	  delay( 100);
	  receiveData( );
	  processData( );
	*/
	
}

void runSlave( ){

	//	Create_Files();

	sprintf(globalLog, "Data_ready: %d",digitalRead(DATA_READY));
	logIt;
	//if the data ready wire is connected, we run the while loop and if loop
	
	while( 1){
		if(digitalRead(DATA_READY)==1){
			sprintf(globalLog, "Data ready detected");
			logIt;
			break;
		}
		
	}
	
	if( digitalRead(DATA_READY)==1){
		Create_Files();
		receiveData( );
		processData( );
	}
	  
}

int main(int argc, char *argv[])
{
	int chan=0;
	int speed= SPI_SPEED; //gives the transfer speed at bits per second
	//	int count=0;
	//   	int defaultSleep=5000;//1000ms
	/*
		if(argc==2){
		defaultSleep = atoi(argv[1]); 
		
		}else if(argc==1){
				defaultSleep=5000;
	}else if( argc != 2 ) // argc should be 2 for correct execution 
		{
			// We print argv[0] assuming it is the program name 
			printf( "usage: sensing sleeptime_in_milliseconds");
		} 
	//   printf("sensing to start\n");
	
	*/
	/*Read the system parameters from amr.conf configuration file*/
	get_system_Param();
	
	/*setup the Wiring Pi library*/
	if (wiringPiSetup () == -1)
		exit (1) ;
	
	/*setup the Wiring Pi SPI driver*/
	if( wiringPiSPISetup (chan, speed)==-1)
		{
			printf("Could not initialise SPIn");
			return 0;
		}

	//Initialize the GPIO pins
	Init_GPIO();


	//qs
	delay( 500);
	digitalWrite(NRST_MB,0); //qs
	printf("reset MB\n");
	delay( 10);
	digitalWrite(NRST_MB,1);
	printf("reset MB finish\n");
	delay( 500);
	/*	while( 1)
		{

			count++;
			printf( "%d\n", count);
			delay( 1000);

		}
	
	*/
	
	//set demux to unuse status
	digitalWrite( USB_DEMUX1,1);
	digitalWrite( USB_DEMUX2,1);
	digitalWrite( USB_DEMUX3,1);
		
    while(1)
	{
		//		printf("inside while loop \n");
		Create_Log_Files( );
		if( master==1)
			runMaster( );
		else
			runSlave( );
		//		run( );
		
		//if the data ready wire is not connected, we need to run this version:
		/*
		    delay(  100);
			  receiveData(  );
			    processData(  );
		*/
	  
		Global_increment = Global_increment +1;

		Close_files();

		if( sampleInterval!=1)
			delay(500);
	}
    return 0;
}

