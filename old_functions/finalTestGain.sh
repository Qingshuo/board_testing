#!/bin/bash
#finalTestGain.sh

lines=0
count=0
now=$(date +"%Y_%m_%d_%H_%M")
sudo mv /tmp/sensingLog "/home/pi/schedule/sensingLog$now"
cd /home/pi/schedule/
tar -jcvf "/home/pi/schedule/sensing/sensingLog$now.tar.bz2" "sensingLog$now"
sudo rm "/home/pi/schedule/sensingLog$now"
sudo rm ~/exp_trace/DBGainResult
sudo rm ~/exp_trace/DBGainAve
sudo rm ~/exp_trace/testDBGainResult
sleep $1
#sleep 3
lines=`grep "DB1 Gain:\[1\]2" /tmp/sensingLog | wc -l`
while [ $lines -lt 1 ]
do
#	sleep 0.01
	lines=`grep "DB1 Gain:\[1\]2" /tmp/sensingLog | wc -l`
done
second=$(date +"%S")
minute=$(date +"%M")
hour=$(date +"%H")
year=$(date +"%Y")
month=$(date +"%m")
day=$(date +"%d")

while true 
do
	let count=$count+1
	if [ $count -lt 41 ]
	then
	    grep "DB1 RMS" /tmp/sensingLog | tail -1 >> ~/exp_trace/DBGainResult

	    
	fi
	
	if [ $count -eq 1 ]
	then
	    echo "receiving gain 2 data"
	fi
	if [ $count -eq 11 ]
	then
	    echo "receiving gain 3 data"
	fi
	if [ $count -eq 21 ]
	then
	    echo "receiving gain 4 data"
	fi
	if [ $count -eq 31 ]
	then
	    echo "receiving gain 5 data"
	fi
	
	if [ $count -eq 40 ]
	then
		break
	fi
    sleep $1
done

awk 'BEGIN{for(i=0;i<12;i++){a2[i]=0;a3[i]=0;a4[i]=0;a5[i]=0;cnt=0;}}//{
if(cnt<10){
for(i=0;i<12;i++){
a2[i]+=$(5+2*i);
}}
else if(cnt<20){
for(i=0;i<12;i++){
a3[i]+=$(5+2*i);
}}
else if(cnt<30){
for(i=0;i<12;i++){
a4[i]+=$(5+2*i);
}}
else if(cnt<40){
for(i=0;i<12;i++){
a5[i]+=$(5+2*i);
}}
cnt++;}
END{for(i=0;i<12;i++){printf "%.3f ", a2[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a3[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a4[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a5[i]/10}
printf "\n";}' ~/exp_trace/DBGainResult > ~/exp_trace/DBGainAve

cat ~/exp_trace/DBGainAve
echo " "
awk 'BEGIN{for(i=1;i<=12;i++){a[i]=0;b[i]=1;g3[i]=0;g4[i]=0;g5[i]=0;cnt=0;}}//{
if(cnt==0){
for(i=1;i<=12;i++){
if($i!=0)
a[i]=$i;
else{
a[i]=1;
b[i]=0;
}
}}
else if(cnt==1){
for(i=1;i<=12;i++){
g3[i]=2*$i/a[i];
if(g3[i]>5.75 || g3[i]<4.25)
{
b[i]=0;
}
}}
else if(cnt==2){
for(i=1;i<=12;i++){
g4[i]=2*$i/a[i];
if(g4[i]>11.5 || g4[i]<8.5)
{
b[i]=0;
}
}}
else if(cnt==3){
for(i=1;i<=12;i++){
g5[i]=2*$i/a[i];
if(g5[i]>23 || g5[i]<17)
{
b[i]=0;
}
}}
cnt++;}
END{
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g3[i])}
printf "\n";
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g4[i])}
printf "\n";
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g5[i])}
printf "\n";
for(i=1;i<=12;i++){
if(b[i]==1) printf ("[%d]pass  ",i);
else printf( "[%d]bad   ",i);
}
printf "\n";}' ~/exp_trace/DBGainAve  > ~/exp_trace/testDBGainResult
cat ~/exp_trace/testDBGainResult


now="$year-$month-$day-$hour-$minute"
sudo mv /tmp/sensingLog /home/pi/exp_trace/sensingLog
sudo mv /tmp/dbgLog /home/pi/exp_trace/dbgLog
cd /home/pi/exp_trace/
tar -jcvf "testDBGain.tar.bz2" "sensingLog" "dbgLog" "DBGainResult" "DBGainAve" "testDBGainResult"
ssh amposerver@192.168.0.2 "mkdir ~/test_gain/$now"
scp "testDBGain.tar.bz2" "amposerver@192.168.0.2:~/test_gain/$now"
rm  "testDBGain.tar.bz2"
sleep 5
ssh amposerver@192.168.0.2 "~/test_gain/./saveFigure.sh $hour $minute $second $1"
#let count1=1
#while [ $count1 -lt 41 ]
#do

#	ssh amposerver@192.168.0.2 "cp /var/www/html/ampo/pi54/16/$month/$day/$hour/figures/${time[$count1]}*  ~/test_gain/$now"
#	let count1=$count1+1
#done

