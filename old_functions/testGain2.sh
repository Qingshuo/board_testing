#!/bin/bash


cat ~/exp_trace/gainAve
echo " "
awk 'BEGIN{for(i=1;i<=12;i++){a[i]=0;b[i]=1;g3[i]=0;g4[i]=0;g5[i]=0;cnt=0;}}//{
if(cnt==0){
for(i=1;i<=12;i++){
if($i!=0)
a[i]=$i;
else{
a[i]=1;
b[i]=0;
}
}}
else if(cnt==1){
for(i=1;i<=12;i++){
g3[i]=2*$i/a[i];
if(g3[i]>5.75 || g3[i]<4.25)
{
b[i]=0;
}
}}
else if(cnt==2){
for(i=1;i<=12;i++){
g4[i]=2*$i/a[i];
if(g4[i]>11.5 || g4[i]<8.5)
{
b[i]=0;
}
}}
else if(cnt==3){
for(i=1;i<=12;i++){
g5[i]=2*$i/a[i];
if(g5[i]>23 || g5[i]<17)
{
b[i]=0;
}
}}
cnt++;}
END{
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g3[i])}
printf "\n";
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g4[i])}
printf "\n";
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g5[i])}
printf "\n";
for(i=1;i<=12;i++){
if(b[i]==1) printf ("[%d]pass  ",i);
else printf( "[%d]bad   ",i);
}
printf "\n";}' ~/exp_trace/gainAve  > ~/exp_trace/testGainResult
cat  ~/exp_trace/testGainResult

now=$(date +"%Y_%m_%d_%H_%M")
sudo mv /tmp/sensingLog /home/pi/exp_trace/sensingLog
sudo mv /tmp/dbgLog /home/pi/exp_trace/dbgLog
cd /home/pi/exp_trace/
tar -jcvf "testGain$now.tar.bz2" "sensingLog" "dbgLog" "gainResult" "gainAve" "testGainResult"




