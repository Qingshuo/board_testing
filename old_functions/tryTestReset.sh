#!/bin/bash
#finalTestReset.sh

lines=0
count=0
now=$(date +"%Y_%m_%d_%H_%M")
sudo mv /tmp/sensingLog "/home/pi/schedule/sensingLog$now"
cd /home/pi/schedule/
tar -jcvf "/home/pi/schedule/sensing/sensingLog$now.tar.bz2" "sensingLog$now"
sudo rm "/home/pi/schedule/sensingLog$now"
sudo rm ~/exp_trace/DBResetResult
sudo rm ~/exp_trace/DBResetAve
sleep 2
#sleep 3
lines=`grep "reset: 1 " /tmp/sensingLog | wc -l`
while [ $lines -lt 1 ]
do
#	sleep 0.01
	lines=`grep "reset: 1 " /tmp/sensingLog | wc -l`
done
second=$(date +"%S")
minute=$(date +"%M")
hour=$(date +"%H")
year=$(date +"%Y")
month=$(date +"%m")
day=$(date +"%d")

while true 
do
	let count=$count+1
	if [ $count -lt 51 ]
	then
	    grep "DB1 RMS" /tmp/sensingLog | tail -1 >> ~/exp_trace/DBResetResult
	fi
	
	if [ $count -eq 1 ]
	then
	    echo "receiving reset 1 data"
	fi
	if [ $count -eq 11 ]
	then
	    echo "receiving reset 5 data"
	fi
	if [ $count -eq 21 ]
	then
	    echo "receiving reset 10 data"
	fi
	if [ $count -eq 31 ]
	then
	    echo "receiving reset 50 data"
	fi
	if [ $count -eq 41 ]
	then
	    echo "receiving reset 100 data"
	fi

	if [ $count -eq 50 ]
	then
		break
	fi
    sleep 2
done

awk 'BEGIN{for(i=0;i<12;i++){a1[i]=0;a5[i]=0;a10[i]=0;a50[i]=0;a100[i]=0;cnt=0;}}//{
if(cnt<10){
for(i=0;i<12;i++){
a1[i]+=$(5+2*i);
}}
else if(cnt<20){
for(i=0;i<12;i++){
a5[i]+=$(5+2*i);
}}
else if(cnt<30){
for(i=0;i<12;i++){
a10[i]+=$(5+2*i);
}}
else if(cnt<40){
for(i=0;i<12;i++){
a50[i]+=$(5+2*i);
}}
else if(cnt<50){
for(i=0;i<12;i++){
a100[i]+=$(5+2*i);
}}
cnt++;}
END{for(i=0;i<12;i++){printf "%.3f ", a1[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a5[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a10[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a50[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a100[i]/10}
printf "\n";}' ~/exp_trace/DBResetResult > ~/exp_trace/DBResetAve

cat ~/exp_trace/DBResetAve
#echo " "

#now="$year-$month-$day-$hour-$minute"
#sudo mv /tmp/sensingLog /home/pi/exp_trace/sensingLog
#sudo mv /tmp/dbgLog /home/pi/exp_trace/dbgLog
#cd /home/pi/exp_trace/
#tar -jcvf "testDBGain.tar.bz2" "sensingLog" "dbgLog" "DBResetResult" "DBResetAve"
#ssh amposerver@192.168.0.2 "mkdir ~/test_reset/$now"
#scp "testDBGain.tar.bz2" "amposerver@192.168.0.2:~/test_reset/$now"
#rm  "testDBGain.tar.bz2"
#sleep 5
#ssh amposerver@192.168.0.2 "~/test_gain/./saveFigure.sh $hour $minute $second 2"




#let count1=1
#while [ $count1 -lt 41 ]
#do

#	ssh amposerver@192.168.0.2 "cp /var/www/html/ampo/pi54/16/$month/$day/$hour/figures/${time[$count1]}*  ~/test_gain/$now"
#	let count1=$count1+1
#done

