#!/bin/bash
#finalTestGain.sh
name=`cat /home/pi/ampo/conf/name`
lines=0
count=0
sudo rm ~/exp_trace/DBGainAve
sudo rm ~/exp_trace/testDBGainResult
lines=`cat ~/exp_trace/gainLog | wc -l`
echo $lines
second=$(date +"%S")
minute=$(date +"%M")
hour=$(date +"%H")
year=$(date +"%Y")
month=$(date +"%m")
day=$(date +"%d")

awk 'BEGIN{for(i=0;i<12;i++){a2[i]=0;a3[i]=0;a4[i]=0;a5[i]=0;cnt=0;}}//{
if(cnt<10){
for(i=0;i<12;i++){
a2[i]+=$(2+2*i);
}}
else if(cnt<20){
for(i=0;i<12;i++){
a3[i]+=$(2+2*i);
}}
else if(cnt<30){
for(i=0;i<12;i++){
a4[i]+=$(2+2*i);
}}
else if(cnt<40){
for(i=0;i<12;i++){
a5[i]+=$(2+2*i);
}}
cnt++;}
END{for(i=0;i<12;i++){printf "%.3f ", a2[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a3[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a4[i]/10}
printf "\n";
for(i=0;i<12;i++){printf "%.3f ", a5[i]/10}
printf "\n";}' ~/exp_trace/gainLog > ~/exp_trace/DBGainAve

cat ~/exp_trace/DBGainAve
echo " "
awk 'BEGIN{for(i=1;i<=12;i++){a[i]=0;b[i]=1;g3[i]=0;g4[i]=0;g5[i]=0;cnt=0;}}//{
if(cnt==0){
for(i=1;i<=12;i++){
if($i!=0)
a[i]=$i;
else{
a[i]=1;
b[i]=0;
}
}}
else if(cnt==1){
for(i=1;i<=12;i++){
g3[i]=2*$i/a[i];
if(g3[i]>5.75 || g3[i]<4.25)
{
b[i]=0;
}
}}
else if(cnt==2){
for(i=1;i<=12;i++){
g4[i]=2*$i/a[i];
if(g4[i]>11.5 || g4[i]<8.5)
{
b[i]=0;
}
}}
else if(cnt==3){
for(i=1;i<=12;i++){
g5[i]=2*$i/a[i];
if(g5[i]>23 || g5[i]<17)
{
b[i]=0;
}
}}
cnt++;}
END{
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g3[i])}
printf "\n";
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g4[i])}
printf "\n";
for(i=1;i<=12;i++){printf ("[%d]%.3f ",i, g5[i])}
printf "\n";
for(i=1;i<=12;i++){
if(b[i]==1) printf ("[%d]pass  ",i);
else printf( "[%d]bad   ",i);
}
printf "\n";}' ~/exp_trace/DBGainAve  > ~/exp_trace/testDBGainResult
cat ~/exp_trace/testDBGainResult


now="$year-$month-$day-$hour-$minute"
sudo mv /tmp/sensingLog /home/pi/exp_trace/sensingLog
sudo mv /tmp/dbgLog /home/pi/exp_trace/dbgLog
cd /home/pi/exp_trace/
tar -jcvf "testDBGain.tar.bz2" "sensingLog" "dbgLog" "gainLog" "nameLog" "DBGainAve" "testDBGainResult"
ssh amposerver@192.168.0.2 "mkdir ~/test_gain/$now"
scp "testDBGain.tar.bz2" "amposerver@192.168.0.2:~/test_gain/$now"
rm  "testDBGain.tar.bz2"
sleep 5
ssh amposerver@192.168.0.2 "~/test_gain/./saveFigure.sh $hour $minute $second $name"
#let count1=1
#while [ $count1 -lt 41 ]
#do

#	ssh amposerver@192.168.0.2 "cp /var/www/html/ampo/pi54/16/$month/$day/$hour/figures/${time[$count1]}*  ~/test_gain/$now"
#	let count1=$count1+1
#done

