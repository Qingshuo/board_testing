#!/bin/bash
#testSPI.sh

sudo rm /tmp/sensingLog
sudo rm ~/exp_trace/SPITmp
sleep 2
lines=0
sleep 50
lines=`grep "DB$1 ByteError" /tmp/sensingLog | wc -l`
echo "total samples: $lines"

grep "DB$1 ByteError" /tmp/sensingLog  >> ~/exp_trace/SPITmp
awk 'BEGIN{cnt=0;}//{
if($4!=0)
{
cnt++;
print $0;
}
}
END{
if(cnt==0){
print "no byte error"}
else{
print cnt, "samples have byte error";}
}' ~/exp_trace/SPITmp


