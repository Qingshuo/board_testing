/*sensing.h*/
#define Demo
#define CHANNELS_TOTAL 9// 12 qs for demo   
/*
 *
 *Preprocessor directives  
 *
*/
#define RESET_LEN 2
#define GAIN_LEN (CHANNELS_TOTAL*2)
#define HEADERBYTE_LEN 4//in bytes
#define HEADER_LEN (HEADERBYTE_LEN+GAIN_LEN+RESET_LEN) // Header + Channel_Gain + Reset, each has two bytes.  
#define DBG_LEN (CHANNELS_TOTAL*6*2)
#define FOOTER_LEN 4 //in bytes  
#define COUNTER_LEN 2  

#define SPI_SPEED 12000000 //in hertz  

//The following directives maps NSS lines of daughter board to the corresponding gpio line in rpi  
#define Synch_Interrupt 8    
#define DATA_READY 4
#define NSS_J6 9     
#define NSS_J7 7   
#define NSS_J8 2//15  
#define NSS_J9 3//16  
#define NSS_J10 0  
#define NSS_MB 1  

#define NSS_J12  21  //G5, pin29
#define NRST_J12 26 //G12, pin32
#define NRST_J6  22  //G6, pin31
#define NRST_J7  23  //G13, pin33
#define NRST_J8  24     //G19, pin35
#define NRST_J9  25      //G26, pin37 
#define NRST_J10 28      //G20, pin38
#define NRST_MB 27       //G16, pin36    
#define USB_DEMUX1 29         //G21, pin40
#define USB_DEMUX2 5         //G24, pin18
#define USB_DEMUX3 6         //G25, pin22
#define DEMUX_UNUSE 100

#define DATA_STARTROW 1    
#define DATA_ENDROW 4801//4801  

#define SPI 1  
#define synch 1
#ifdef Demo
#define SPI_RX_TIMES 4
#else
#define SPI_RX_TIMES 4//4 qs for demo
#endif

#define headerBYTE1 0xf1  
#define headerBYTE2 0xf2  
#define headerBYTE3 0xf3  
#define headerBYTE4 0xf4  

#define footerBYTE1 0xe1  
#define footerBYTE2 0xe2  
#define footerBYTE3 0xe3  
#define footerBYTE4 0xe4  

#define MASTER


/*
 *
 *Define for MB
 *
*/
#ifdef Demo
#define SAMPLES_MB (9000*2) //bytes
#else
#define SAMPLES_MB (4800*2) //bytes
#endif

#ifdef Demo
#define Total_CT 6//3
#define DATA_LEN_MB ( Total_CT*SAMPLES_MB)
#define GAIN_LEN_MB (Total_CT*2)
#define DBG_LEN_MB (Total_CT*6*2)
#else
#define Total_CT 8
#define DATA_LEN_MB ( CHANNELS_TOTAL*SAMPLES_MB)
#define GAIN_LEN_MB (CHANNELS_TOTAL*2)
#define DBG_LEN_MB (CHANNELS_TOTAL*6*2)
#endif

#define GAIN_DATA_LEN_WITH_DBG_MB GAIN_LEN_MB+DATA_LEN_MB+DBG_LEN_MB //in bytes  
#define  buff_len_mb (HEADERBYTE_LEN+GAIN_DATA_LEN_WITH_DBG_MB+COUNTER_LEN+RESET_LEN+FOOTER_LEN)  
#define MB_INDEX 0

/*
 *
 *Define for DB
 *
*/
//#define CHANNELS_TOTAL 15  

#define Max_DB 6 //qs
#define SAMPLES_DB ( 6000*2 )//qs from 4800 changed to 6000 for demo set; from 4800 changed to 3800 for the case of 15 sensors
#define DATA_LEN_DB (CHANNELS_TOTAL*SAMPLES_DB)
#define GAIN_DATA_LEN_WITH_DBG_DB GAIN_LEN+DATA_LEN_DB+DBG_LEN //in bytes 
#define  buff_len  (DATA_LEN_DB+ HEADER_LEN+DBG_LEN+COUNTER_LEN+FOOTER_LEN) 
#define DATA_LEN_WITH_DBG_COUNTER DATA_LEN_DB+DBG_LEN+COUNTER_LEN //in bytes  
/*
 *
 *Define for LOG and DBG
 *
 */
#define DBG_STARTROW 4802  //4802
#define DBG_ENDROW 4807  //4806                           //4807
#define DEBUG_PRINT_BYTES 200//100
#define Log_File_Length 1 //Log_File_Length*1000 SAMPLES per sensor will be logged in one file  
#define logIt writeLog(globalLog)  
#define dbgIt writeDbgLog(globalDbg)


//MB
#ifdef Demo
typedef struct {

	uint16_t Channel_Gain[ Total_CT];
	uint16_t Sampled_data[ SAMPLES_MB/2][ Total_CT];

	uint16_t channelMax[ Total_CT];
	uint16_t channelMin[ Total_CT];
	uint16_t channelDiff[ Total_CT];
	uint16_t reasonOne[ Total_CT];
	uint16_t reasonTwo[ Total_CT];
	uint16_t reasonThree[ Total_CT];

	uint16_t version;
	uint8_t counter;
	uint8_t numReset;
}Control_Buff_MB;

#else
typedef struct {

	uint16_t Channel_Gain[ CHANNELS_TOTAL];
	uint16_t Sampled_data[ SAMPLES_MB/2][ CHANNELS_TOTAL];

	uint16_t channelMax[ CHANNELS_TOTAL];
	uint16_t channelMin[ CHANNELS_TOTAL];
	uint16_t channelDiff[ CHANNELS_TOTAL];
	uint16_t reasonOne[ CHANNELS_TOTAL];
	uint16_t reasonTwo[ CHANNELS_TOTAL];
	uint16_t reasonThree[ CHANNELS_TOTAL];

	uint16_t version;
	uint8_t counter;
	uint8_t numReset;
}Control_Buff_MB;

#endif

//DB
typedef struct {

	uint16_t Channel_Gain[ CHANNELS_TOTAL];
	uint16_t Sampled_data[ SAMPLES_DB/2][ CHANNELS_TOTAL];//assume DB and MB have the same data lenght. If different, need another buff

	uint16_t channelMax[ CHANNELS_TOTAL];
	uint16_t channelMin[ CHANNELS_TOTAL];
	uint16_t channelDiff[ CHANNELS_TOTAL];
	uint16_t reasonOne[ CHANNELS_TOTAL];
	uint16_t reasonTwo[ CHANNELS_TOTAL];
	uint16_t reasonThree[ CHANNELS_TOTAL];

	uint16_t version;
	uint8_t counter;
	uint8_t numReset;
}Control_Buff;
